#!/usr/bin/env bash
#
# Compile OPAL and run unit- and regression tests.
#
# Usually this script is called via CRON or a CD/CI pipeline without any
# argument.
#
# You can call this script from the command line anyway. The only excepted
# option is '--publish-dir' to override the default publish directory. If
# you use '--publish-dir=', publishing is disabled.
#
# If you don't want to run all tests, the name of the tests can be passed
# as arguments.

set -o errexit

declare -ri	EC_ARG_ERROR=1
declare -ri	EC_GIT_ERROR=10

# for Macports
# :FIXME: do we need this?
#         YES:	as long as readlink(1) shipped with Mac OS X does not
#	 	support the option '-f'
PATH+=":/opt/local/bin:/usr/local/bin"

# for Mac OS
declare -rx	MACOSX_DEPLOYMENT_TARGET=10.12

#
# default branch for OPAL sources and regression tests
declare		branch='master'
declare         regtests_branch=''

#..............................................................................
# arguments passed to CMake
#
declare -a cmake_args=()

#..............................................................................
# arguments passed to OPAL
declare -a opal_args=()

#..............................................................................
# arguments passed to OPAL unit tests
declare unittests_args=()
#
# end of configuration
#-----------------------------------------------------------------------------

declare -r today=$(date +%Y-%m-%d)
declare -r time=$(date +%H-%M)
declare -r timestamp="${today}_${time}"

declare aborting='yes'

trap _signal_handler SIGINT SIGTERM
_signal_handler() {
	local ec=$?
	echo "Aborting ..."
	aborting='yes'
	exit ${ec}
}

trap _exit EXIT
_exit() {
        local -i ec=$?
	if [[ ${aborting} == 'yes' ]]; then
		return ${ec}
	fi
	update_overview
}

die() {
	(( $1 > 0 )) && exec 1>&2
	echo -e "$2\n"
	exit $1
}

usage() {
	echo "Usage:
	$(basename "$0") [OPTIONS...] [TESTS...]

Compile OPAL, run unit- and regression-tests. The regression-tests
to run can be passed as arguments on the command line. If no tests
are passed all tests are performed.

On the first time this script is called, the following steps will be
performed:

1. clone OPAL repository
2. compile OPAL, stop on error
3. run unit-tests, stop on error
4. clone regression-tests repository
5. run regression-tests

On further calls steps 2., 3. and 5. are only performed, if something
changed:

1. update OPAL repository
2. compile OPAL only if changed, stop on error
3. run unit-tests only if OPAL has been compiled, stop on error
4. update regression-tests repository
5. run regression-tests only if OPAL has been compiled or the some 
    regression-tests has been changed.


OPTIONS:
===========
--src-repo-url=URL
	URL of the OPAL repository to use. This option must be set
	if the tests should be run on a fork of the OPAL repository
	https://gitlab.psi.ch/OPAL/src.

--branch=BRANCH
	Branch in the source repository to use.

--tests-repo-url=URL
	URL of the regression-tests repository. This option must 
	be set, if the tests should in the fork of the tests in
	https://gitlab.psi.ch/OPAL/regression-tests

--regtests-branch=BRANCH
	Branch in the regression-tests repository to use.

--cmake-args=ARG
	Argument/option passed to CMake. This option can be used
	multiple times.

--opal-args=ARG
	Argument/option passed to OPAL. This option can be used
	multiple times.
	
--unit-tests-args=ARGS
	Argument/options passed to the unit-tests. This option can
	for example be used to disable broken unit-tests.

--publish-dir=DIR
	Prefix of the publication directory. If not set
	or empty, no HTML output will be created.

--config=FILE
	The options above can be defined in the configuration FILE.
	See section CONFIG FILE for more details.
	
--compile
	Compile OPAL even nothing changed and a binary exists.

--unit-tests
	Run unit-tests even OPAL has not been compiled.

--reg-tests
	Run regression-tests even OPAL has not been compiled and the
	tests has not been changed.

-f | --force
	Force compiling OPAL and run unit- and regression-tests.

-h | -? | --help
	This help.


CONFIG FILE
===========
src_repo_url=URL
	See option --src-repo-url.

branch=BRANCH
	See option --branch.

tests_repo_url=URL
	See option --test-repo-url.

regtests_branch=BRANCH
	See option --regtests-branch.

cmake_args
	See option --cmake-args.
	In the configuration file 'cmake_args' is a BASH array. The
	recommended way to define CMake options in a configurarion 
	file is

	cmake_args+=( "ARG1" )
	cmake_args+=( "ARG2" )
        ...

opal_args
	See option --opal-args.
	The syntax is the same as for 'cmake_args'

unit_tests_args=ARGS
	See option --unit-tests-args.

publish_dir=DIR
	See option --publish-dir
" 1>&2
	exit 1
}

#
#-----------------------------------------------------------------------------
# handle arguments
#

# default URL for OPAL source and regression-tests repository
declare		src_repo_url='https://gitlab.psi.ch/OPAL/src.git'
declare		tests_repo_url='https://gitlab.psi.ch/OPAL/regression-tests.git'


# absolute or relative directory where to publish the results
# of the regression und unit tests. If empty, nothing will be
# published.
declare -x	publish_dir=''

# run unit- and/or regression tests
declare		opt_force='no'
declare		do_compile='no'
declare         do_unittests='no'
declare         do_regtests='no'

# configuration file
declare		opt_config_file="default-${branch}.conf"

# tests to run, all if non passed as argument
declare -a      tests=()

while (( $# > 0 )); do
	case $1 in
                --branch|--branch=* )
                        if [[ $1 == *=* ]]; then
                                branch="${1#*=}"
                        else
                                branch="$2"; shift 1
                        fi
                        ;;
                --regtests-branch|--regtests-branch=* )
                        if [[ $1 == *=* ]]; then
                                regtests_branch="${1#*=}"
                        else
                                regtests_branch="$2"; shift 1
                        fi
                        ;;                         
		--cmake-args|--cmake-args=* )
                        if [[ $1 == *=* ]]; then
		                cmake_args+=( "${1#*=}" )
                        else
			        cmake_args+=( "$2" ); shift 1
		        fi
		        ;;
                --config|--config=* )
                        if [[ $1 == *=* ]]; then
		                opt_config_file="${1#*=}"
                        else
			        opt_config_file="$2"; shift 1
                        fi
                        if [[ ! -e "${opt_config_file}" ]]; then
                                die 1 "ERROR: configuration file does not exist - ${opt_config_file}"
                        fi
                        source "${opt_config_file}"
                        if (( $? != 0)); then
                                die 1 "ERROR: cannot source configuration file - ${opt_config_file} "
                        fi

                        ;;
		--opal-args|--opal-args=* )
                        if [[ $1 == *=* ]]; then
			        opal_args+=( "${1#*=}" )
                        else
			        opal_args+=( "$2" ); shift 1
                        fi
			;;
		--output-dir|--output-dir=* )
                        if [[ $1 == *=* ]]; then
				output_dir="${1#*=}"
                        else
			        output_dir="$2"; shift 1
                        fi
			mkdir -p "${output_dir}"
			ofile="${output_dir}/${timestamp}.txt"
			exec 1> "${ofile}" 2>&1
			;;
		--publish-dir|--publish-dir=* )
                        if [[ $1 == *=* ]]; then
			        publish_dir="${1#*=}"
                        else
		                publish_dir="$2"; shift 1
                        fi
                        if [[ -n ${publish_dir} ]]; then
                                publish_dir=$(readlink -f "${publish_dir}")
                        fi
		        ;;
		--src-repo-url|--src-repo-url=* )
                        if [[ $1 == *=* ]]; then
		                src_repo_url="${1#*=}"
                        else
		                src_repo_url="$2"; shift 1
                        fi
		        ;;
		--tests-repo-url|--tests-repo-url=* )
                        if [[ $1 == *=* ]]; then
		                tests_repo_url="${1#*=}"
                        else
		                tests_repo_url="$2"; shift 1
                        fi
		        ;;
                --unit-tests-args|--unit-tests-args=* )
                        if [[ $1 == *=* ]]; then
                                unittests_args+=( "${1#*=}" )
                        else
                                unittests_args+=( "$2" ); shift 1
                        fi
                        ;;
		--compile )
			do_compile='yes'
			;;
                --reg-tests )
                        do_regtests='yes'
                        ;;
                --unit-tests )
                        do_unittests='yes'
			;;
		-f | --force )
		        opt_force='yes'
			do_compile='yes'
                        do_regtests='yes'
                        do_unittests='yes'
		        ;;
		-h | -\? | --help )
			usage
			;;
		-* )
		        echo "Unknown option: $1" 1>&2
		        exit
		        ;;
		* )
		        tests+=( "$1" )
		        ;;
	esac
	shift 1
done

: ${regtests_branch:=${branch}}

mkdir -p "${publish_dir}"

#.............................................................................
# directory layout
#
# ${PREFIX}/scripts					${bindir}
# ${PREFIX}/workspace					${workspace}
# ${PREFIX}/workspace/${branch}				${branchdir}
# ${PREFIX}/workspace/${branch}/src			${srcdir}
# ${PREFIX}/workspace/${branch}/build			${builddir}
# ${PREFIX}/workspace/${branch}/tests			${testsbase}
# ${PREFIX}/workspace/${branch}/tests/RegressionTests	${testsdir}

declare -r abs_scriptname=$(readlink -f "$0")

declare -r bindir="${abs_scriptname%/*}"
declare -r prefix="${bindir%/*}"
declare -r workspace="${prefix}/workspace"
declare -r branchdir="${workspace}/${branch}"
declare -r srcdir="${branchdir}/src"
declare    builddir="${branchdir}/build"
declare -r testsbase="${branchdir}/tests"
declare -r testsdir="${testsbase}/RegressionTests"

declare -A result
result['compile']='-'
result['u_total']='-'
result['u_passed']='-'
result['u_failed']='-'
result['u_disabled']='-'
result['u_broken']='-'
result['r_total']='-'
result['r_passed']='-'
result['r_failed']='-'
result['r_disabled']='-'
result['r_broken']='-'

clone_repo() {
	local -r repo_url="$1"
	local -r dir="$2"
	{
		git clone "${repo_url}" "${dir}"|| exit ${EC_GIT_ERROR}
		cd "${dir}"
		git checkout "${branch}" 	|| exit ${EC_GIT_ERROR}
	}
}

update_repo() {
	local -r _dir="$1"
	local -r _branch="$2"
	local -r _url="$3"

	[[ -n "${_dir}" ]]    || die ${EC_ARG_ERROR} "Oops in $0: no repo dir passed!"
	[[ -n "${_branch}" ]] || die ${EC_ARG_ERROR} "Oops in $0: no branch name passed!"
	[[ -n "${_url}" ]]    || die ${EC_ARG_ERROR} "Oops in $0: no repo URL passed!"

	if [[ ! -d "${_dir}" ]]; then
		git clone "${_url}" "${_dir}"
		cd "${_dir}"
		git checkout "${_branch}"
		return 0
	else
		cd "${_dir}"
		git rev-parse --is-inside-work-tree 1>/dev/null 2>&1 || \
			die ${EC_GIT_ERROR} "${_dir}: is not a Git working directory!"
		git checkout "${_branch}"
		git fetch

		local -i num_changed_files=$(git diff --name-only  "origin/${_branch}"  | wc -l | awk '{print $1}')
		if (( $num_changed_files == 0 )); then
			echo "Working directory '${_dir}' is up to date!"
			return 1
		fi
		git merge "origin/${_branch}"
		return 0
	fi
}



compile_opal() {
	echo "Compiling OPAL ..."
        result['compile']='failed'
	cd "${branchdir}"
	rm -rf "${builddir}"
	mkdir -p "${builddir}"
	cd "${builddir}"
	cmake "${cmake_args[@]}" "${srcdir}"
	time make -j5
        result['compile']='ok'
}

run_unit_test() {
        # run unit-test only if OPAL has been compiled
        if [[ ${result['compile']} == '-' ]]; then
                echo "OPAL has not been rebuild: skipping unit-tests."
                return 0
        fi
        # compile with unit-tests enabled?
        local -r tests_bin="${builddir}/tests/opal_unit_tests"
        if [[ ! -x "${tests_bin}" ]]; then
                echo "Compiled without Gtest: skipping unit-tests."
                return 0
        fi
        echo "Running unit tests..."

        if [[ -n "${publish_dir}" ]]; then
                local -r publish_unittests_dirs="${publish_dir}/unitTests/${branch}"
                mkdir -p "${publish_unittests_dirs}"
                local output_file="${publish_unittests_dirs}/results_${timestamp}.xml"
                unittests_args+=( "--gtest_output=xml:${output_file}" )
        fi

        local -i ec=0
        ${tests_bin} "${unittests_args[@]}" || ec=$?

        [[ -n ${publish_dir} ]] || return ${ec}

        # add stylesheet to output
        sed -n -i 'p;1a <?xml-stylesheet type="text/xsl" href="results.xsl"?>' \
            "${output_file}" 

        local -i tests
        local -i failures
        local -i disabled
        local -i errors

        eval $(awk '/<testsuites / {printf "%s %s %s %s\n", $2, $3, $4, $5}' \
                   "${output_file}")

        test_overview="[ total: ${tests} "
        test_overview+="| failures: ${failures} "
        test_overview+="| disabled: ${disabled} "
        test_overview+="| errors: ${errors} ] <br/>"

        result['u_total']=${tests}
        result['u_passed']=$((tests - failures - disabled - errors))
        result['u_failed']=${failures}
        result['u_disabled']=${disabled}
        result['u_broken']=${errors}

        line="<a href=\"${output_file##*/}\">${timestamp}</a> ${test_overview}"

        if [[ ! -e "${publish_unittests_dirs}/index.html" ]]; then
	        echo "<head>
  <body>
  </body>
</head>
" > "${publish_unittests_dirs}/index.html"
        fi
        if [[ ! -e "${publish_unittests_dirs}/result.xsl" ]]; then
	        cp -v \
                   "${bindir}/html/gtest-result.xsl" \
                   "${publish_unittests_dirs}/results.xsl"
        fi
        sed -i "/  <body>/a ${line}" "${publish_unittests_dirs}/index.html"
        return ${ec}
}

run_regressiontests() {
	echo "Running regression tests..."
        local -r run_tests="${bindir}/run-reg-tests.py"

        local opts=()
        if [[ -n "${publish_dir}" ]]; then
                local -r publish_regtests_dir="${publish_dir}/regressionTests/${branch}"
	        mkdir -p "${publish_regtests_dir}"
	        opts+=("--publish-dir=${publish_regtests_dir}")
        fi

	if [[ -n "${opal_args}" ]]; then
		opts+=(${opal_args[@]/#/--opal-args=})
	fi
	opts+=("--opal-exe-path=${builddir}/src")
	opts+=("--base-dir=${testsdir}")
        opts+=("--timestamp=${timestamp}")
	"${run_tests}" "${opts[@]}" "--" "${tests[@]}"
        local -i ec=$?

        [[ -n ${publish_dir} ]] || return ${ec}

        # get results from HTML file created/updated by regression-tests 
        local index_html="${publish_regtests_dir}/index.html"
        read passed broken failed total < <(
                awk "/${timestamp}/ {
                     split(\$4, p, \":\");
                     split(\$6, b, \":\");
                     split(\$8, f, \":\");
                     split(\$10, t,  \":\");
                     printf \"%s %s %s %s\n\", p[2], b[2], f[2], t[2]; exit}" \
                             < ${index_html})
        result['r_total']=${total/\]}
        result['r_passed']=${passed}
        result['r_failed']=${failed}
        result['r_disabled']='-'
        result['r_broken']=${broken}

        return ${ec}
}

update_overview() {

        # nothing to do if ...
        [[ -n ${publish_dir} ]] || return ${ec}
	[[ "${result['compile']}${result['u_total']}${result['r_total']}" == '---' ]] && return ${ec}

	mkdir -p "${publish_dir}/${branch}"

        # org file with the table we have to update
        local -r index_org="${publish_dir}/${branch}/index.org"

        # copy template to target directory if not exist
        if [[ ! -e "${index_org}" ]]; then
	        sed "s/@BRANCH@/${branch}/" \
                   "${bindir}/html/index.org" > "${index_org}"
        fi

        # links to generated XML pages used in the table
        local -r u_results_xml="../unitTests/${branch}/results_${timestamp}.xml"
        local -r r_results_xml="../regressionTests/${branch}/results_${timestamp}.xml"

	local time_col
	local ofile="${publish_dir}/${branch}/output/${timestamp}.txt"
	if [[ -e "${ofile}" ]]; then
		time_col="[[./output/${timestamp}.txt][${today} ${time/-/:}]]"
	else
		time_col="${today} ${time/-/:}"
	fi

        # build string with result for unit-tests. This will be used as link-name.
        if [[ "${result['u_total']}" == "-" ]]; then
                local -r u_report='-'
        else
                local -r u_t="total:${result['u_total']}"
                local -r u_p="passed:${result['u_passed']}"
                local -r u_f="failed:${result['u_failed']}"
                local -r u_d="disabled:${result['u_disabled']}"
                local -r u_b="broken:${result['u_broken']}"
                local -r u_report="[[${u_results_xml}][${u_t}; ${u_p}; ${u_f}; ${u_d}; ${u_b}]]"
        fi
        # build string with result for regression-tests. This will be used as link-name.
        if [[ "${result['r_total']}" == "-" ]]; then
                local -r r_report='-'
        else
                local -r r_t="total:${result['r_total']}"
                local -r r_p="passed:${result['r_passed']}"
                local -r r_f="failed:${result['r_failed']}"
                local -r r_d="disabled:${result['r_disabled']}"
                local -r r_b="broken:${result['r_broken']}"
                local -r r_report="[[${r_results_xml}][${r_t}, ${r_p}, ${r_f}, ${r_d}, ${r_b}]]"
        fi
        # add new line to org table
        line=""
        line+="| ${time_col} "
        line+="| ${result['compile']} "
        line+="| ${u_report} "
        line+="| ${r_report} "
        line+="|"
        local -r tmpfile=$(mktemp "/tmp/awk.XXXXXX")
        awk "{print}; /^\|---/ {print \"${line}\"}" \
            ${index_org} > "${tmpfile}"
        mv "${tmpfile}" "${index_org}"

	emacs "${index_org}" --batch -f org-html-export-to-html --kill
        #emacs --batch --eval "
        #       (with-current-buffer (find-file-noselect \"${index_org}\")
        #          (org-html-export-as-html)
        #          (write-file \"${index_org%.*}.html\"))"
}

#
# compile OPAL if something changed or forced
# run unit-tests if OPAL binary is new or forced
# run regression tests if OPAL binary is new, regression tests have been changed or forced
#
# compile
# compile, unit-tests
# compile, unit-tests, regression-tests
# regression-tests
#
# Options:
# --compile: compile even binary is available and nothing changed, run unit- and regression-tests
# --unit-tests: compile if something changed or no binary, run unit- und regression-tests
# --reg-tesrs: like --unit-tests, run regression-tests with existing or new binary
# --force: compile and run unit- and regression-tests


main() {
	local -r opalbin="${builddir}/src/opal"

	#.....................................................................
	echo "Updating OPAL source repository..."
	update_repo "${srcdir}" "${branch}" "${src_repo_url}" && do_compile='yes'

	[[ -e ${opalbin} ]] || do_compile='yes'
	aborting='no'
	if [[ "${do_compile}" == "yes" ]]; then
		compile_opal
		do_unittests='yes'
		do_regtests='yes'
	fi
        if [[ ${do_unittests} == 'yes' ]]; then
		run_unit_test || :
        fi
 
	echo "Updating regression tests repository..."
	update_repo "${testsbase}" "${regtests_branch}" "${tests_repo_url}" && do_regtests='yes'

        if [[ ${do_regtests} == 'yes' ]]; then
		run_regressiontests || return 1
        fi
}

main

# Local Variables:
# mode:sh-mode
# sh-basic-offset: 8
# indent-tabs-mode: nil
# require-final-newline: nil
# End:
