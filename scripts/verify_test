#!/usr/bin/env python

import sys
import os
import datetime
#import tools
#import OpalRegressionTests

from OpalRegressionTests.reporter import Reporter
from OpalRegressionTests.reporter import TempXMLElement

import OpalRegressionTests.stattest as stattest
import OpalRegressionTests.outtest as outtest
import OpalRegressionTests.lbaltest as lbaltest
import OpalRegressionTests.losstest as losstest

class RegressionTest:
    def __init__(self, name):
        self.dir = os.path.abspath(name)
        self.name = os.path.basename(name)
        self.basename = os.path.join(self.dir, self.name, self.name)
        self.totalNrPassed = 0
        self.totalNrTests = 0
        
    def performTests(self, root):
        rep = Reporter()
        fname = os.path.join(self.dir, self.name + ".rt")
        with open(fname,"r") as infile:
            tests = [line.rstrip('\n') for line in infile]

        description = tests[0].lstrip("\"").rstrip("\"")
        rep.appendChild(root)
        tests = tests[1::] #strip first line
        for i, test in enumerate(tests):
            try:
                self.totalNrTests += 1
                test_root = TempXMLElement("Test")
                passed = self.checkTest(test, test_root)
                if passed:
                    self.totalNrPassed += 1
                    root.appendChild(test_root)
            except Exception:
                exc_info = sys.exc_info()
                sys.excepthook(*exc_info)
                rep.appendReport(
                    ("Test broken: didn't succeed to parse %s.rt file line %d\n"
                     "%s\n"
                     "Python reports\n"
                     "%s\n\n") % (self.name, i+2, test, exc_info[1])
                )

    """
    handler for comparison of various output files with reference files

    Note that we do something different for loss tests as the file name in
    general is not <name>.loss, rather it is <element_name>.loss
    """
    def checkTest(self, test, root):
        nameparams = str.split(test,"\"")
        var = nameparams[1]
        params = str.split(nameparams[2].lstrip(), " ")
        rtest = 0
        file_name, file_extension = os.path.splitext(test.split()[0])
        if "stat" in test:
            rtest = stattest.StatTest(var, params[0], float(params[1]), self.dir, self.name)
        elif "out" in test:
            rtest = outtest.OutTest(var, params[0], float(params[1]), self.dir, self.name)
        elif "lbal" in test:
            rtest = lbaltest.LbalTest(var, params[0], float(params[1]), self.dir, self.name)
        elif file_extension == ".loss":
            rtest = losstest.LossTest(var, params[0], float(params[1]), test.split()[0])
        elif file_extension == ".smb":
            rtest = stattest.StatTest(var, params[0], float(params[1]), file_name, file_extension)
        else:
            rep = Reporter()
            rep.appendReport("Error: unknown test type %s\n" % nameparams[0])
            return False

        return rtest.checkResult(root)

if __name__ == "__main__":
    name = sys.argv[1]

    test = RegressionTest(name)
    rep = Reporter()
    report = TempXMLElement("Simulation")
    report.addAttribute("name", name)
    report.addAttribute("date", "%s" % (datetime.date.today()))

    test.performTests(report)
    print (rep.getReport())
